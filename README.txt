INTRODUCTION:
-------------
* In multi domain site this module automatically create new users or update 
  old user's data on the other sites if it is created/updated on any one site.


REQUIREMENTS:
------------
No special requirements.


Recommended modules:
--------------------
None.


INSTALLATION:
-------------
* Install as usual, see http://drupal.org/node/895232 for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.
